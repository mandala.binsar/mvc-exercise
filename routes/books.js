var express = require("express");
var router = express.Router();

const bookController = require("../controllers/bookController");

router.get("/", bookController.getBooks);
router.get("/:id", bookController.getBook);
router.post("/", bookController.postBook);
router.put("/:id", bookController.putBook);
router.delete("/:id", bookController.deleteBook);

module.exports = router;
