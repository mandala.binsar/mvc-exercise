var express = require("express");
var router = express.Router();

const indexController = require("../controllers/indexController");

router.get("/", indexController.landingPage);
module.exports = router;

const bookController = require("../controllers/bookController");
router.get("/tes", bookController.getBooks);
