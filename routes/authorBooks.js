var express = require("express");
var router = express.Router();

const authorBooksController = require("../controllers/authorBooksController");
router.post("/", authorBooksController.postAuthorBooks);
router.get("/", authorBooksController.getAuthorBooks);
router.get("/:id", authorBooksController.getAuthorBooksById);
router.delete("/:id", authorBooksController.deleteAuthorBooks);

module.exports = router;
