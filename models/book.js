"use strict";
const { Model } = require("sequelize");
//const author = require("./author");
module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //Book.belongsTo(models.Author, { foreignKey: "author_id" });
      /*
      Book.belongsTo(models.Author, {
        foreignKey: "id",
      });
      */

      this.belongsTo(models.Author, { foreignKey: "author_id", as: "author" });
    }
  }
  Book.init(
    {
      title: DataTypes.STRING,
      publish_date: DataTypes.DATE,
      author_id: DataTypes.INTEGER,
      description: DataTypes.STRING,
      publisher: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Book",
    }
  );
  return Book;
};
