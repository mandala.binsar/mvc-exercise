const { Author, Book } = require("../models");

/*
exports.getAuthorBooks = (req, res) => {
  Author.findOne({
    where: { id: req.params.id },
    include: [{ model: book }],
  });
};
*/

/*
let getAuthorBooks = Author.findOne({
  where: { id: req.params.id },
  include: [{ model: Book }],
});
*/

//still error
exports.getAuthorBooks = async (req, res) => {
  try {
    authors = await Author.findAll({
      include: [
        {
          model: Book,
          as: "books",
        },
      ],
    });

    res.status(200).json({
      status: true,
      message: "All Author retrieved",
      data: authors,
    });
  } catch (error) {
    res.json(error);
  }
};

exports.getAuthorBooksById = async (req, res) => {
  try {
    const authorID = req.params.id || req.body.id;
    authors = await Author.findByPk(authorID, {
      include: [
        {
          model: Book,
          as: "books",
        },
      ],
    });

    res.status(200).json({
      status: true,
      message: `Author with ID ${req.params.id} retrieved with it's books`,
      data: authors,
    });
  } catch (error) {
    res.json(error);
  }
};

exports.postAuthorBooks = async (req, res) => {
  try {
    const author = await Author.create({
      name: req.body.name,
      birth_date: req.body.birth_date,
    });

    const book = await Book.create({
      title: req.body.books.title,
      publish_date: req.body.books.publish_date,
      author_id: author.id,
      description: req.body.books.description,
    });

    /*
    await postBook({
      title: req.body.books.title,
      publish_date: req.body.books.publish_date,
      author_id: author.id,
      description: req.body.books.description,
    });
    */
    authors = await Author.findByPk(author.id, {
      include: [
        {
          model: Book,
          as: "books",
        },
      ],
    });

    res.status(200).json({
      status: true,
      message: `Author with books created`,
      data: authors,
    });
  } catch (error) {
    res.json(error);
  }
};

exports.deleteAuthorBooks = async (req, res) => {
  try {
    authorID = req.params.id;

    await Book.destroy({
      where: {
        author_id: req.params.id,
      },
    });

    await Author.destroy({
      where: {
        id: req.params.id,
      },
    });

    res.status(204).json({
      status: true,
      message: `Author with ID ${req.params.id} deleted`,
    });
  } catch (error) {
    res.json(error);
  }
};
